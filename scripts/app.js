(function () {
    'use strict';

    var app = angular.module(
        'app',
        [
            'ngRoute',
            'ngMaterial',
            'ngAnimate'
        ]
    );
    app.config(
        [
            '$routeProvider',
            function ($routeProvider) {
                $routeProvider.when(
                    '/', {
                        templateUrl: './scripts/home/home.html',
                        controller: 'home'           
                    }
                )
                    .when('/musicHome', {
                    templateUrl: './scripts/dashboard.html',
                    controller: 'dashboard'
                });
                $routeProvider.otherwise({redirectTo: '/'});
            }
        ]
    );
})();