angular.module('app')

    .controller('home', function ($scope, $timeout, $mdSidenav, $log) {
    
    console.log("Inside Home");
    $scope.toggleLeft = buildDelayedToggler('left');
    $scope.toggleRight = buildToggler('right');
    $scope.isOpenRight = function(){
        return $mdSidenav('right').isOpen();
    };
    
    var fs = require('fs');
    console.log(fs);
    var tit=[];
    fs.readdir('songs', (err, dir) => {
        console.log(dir);
        for(let filePath of dir){
            console.log(filePath);
            tit.push(filePath);
        }

    });

    function debounce(func, wait, context) {
        var timer;

        return function debounced() {
            var context = $scope,
                args = Array.prototype.slice.call(arguments);
            $timeout.cancel(timer);
            timer = $timeout(function() {
                timer = undefined;
                func.apply(context, args);
            }, wait || 10);
        };
    }

    /**
     * Build handler to open/close a SideNav; when animation finishes
     * report completion in console
     */
    function buildDelayedToggler(navID) {
        return debounce(function() {
            // Component lookup should always be available since we are not using `ng-if`
            $mdSidenav(navID)
                .toggle()
                .then(function () {
                $log.debug("toggle " + navID + " is done");
            });
        }, 200);
    }

    function buildToggler(navID) {
        return function() {
            // Component lookup should always be available since we are not using `ng-if`
            $mdSidenav(navID)
                .toggle()
                .then(function () {
                $log.debug("toggle " + navID + " is done");
            });
        };
    }
    $scope.todos = [
        {
            
            what: 'Brunch this weekend?',
            who: 'Min Li Chan',
            when: '3:08PM',
            notes: " I'll be in your neighborhood doing errands"
        },
        {
            
            what: 'Brunch this weekend?',
            who: 'Min Li Chan',
            when: '3:08PM',
            notes: " I'll be in your neighborhood doing errands"
        },
        {
            
            what: 'Brunch this weekend?',
            who: 'Min Li Chan',
            when: '3:08PM',
            notes: " I'll be in your neighborhood doing errands"
        }
    ];
    $scope.currentSongList=['songs/lamhe.mp3'];
    $scope.close = function () {
        // Component lookup should always be available since we are not using `ng-if`
        var sound = new Howl({
            src: $scope.currentSongList
        });
        $scope.soundID=sound.play();
        console.log( $scope.soundID);

    };
    $scope.nextSong = function () {
        var requestSong='songs/dhere.mp3';
        var i=tit.indexOf(requestSong);
        sound.stop($scope.soundID);
        // Component lookup should always be available since we are not using `ng-if`
        var sound = new Howl({
            src: tit[i]
        });
        $scope.soundID=sound.play();
        console.log(sound.play());

    };
    // ===== Open Nav =====
    
    
})


   
    .controller('RightCtrl', function ($scope, $timeout, $mdSidenav, $log) {
    $scope.close = function () {
        // Component lookup should always be available since we are not using `ng-if`
        $mdSidenav('right').close()
            .then(function () {
            $log.debug("close RIGHT is done");
        });
    };
});